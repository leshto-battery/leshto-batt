#!/bin/bash

ID_USUARIO=0
if [[ $(id -u) -eq $ID_USUARIO ]]; then
  echo "Não instale em modo root OU troque a ID_USUARIO do script para um número diferente de 0!"
  exit 0
fi

echo "Instalando..."
mkdir -p $HOME/.local/{opt,bin}/
cp -r src $HOME/.local/opt/leshto-batt
cp remove.sh $HOME/.local/opt/leshto-batt/
cp LICENSE $HOME/.local/opt/leshto-batt/
chmod +x $HOME/.local/opt/leshto-batt/runner.py
chmod +x $HOME/.local/opt/leshto-batt/remove.sh
ln -s $HOME/.local/{opt/leshto-batt/runner.py,bin/lebatt}
cp lebatt.desktop $HOME/.local/share/applications/

echo "Excluindo lixo..."
find $HOME/.local/opt/leshto-batt -name '*.pyc' -delete
find $HOME/.local/opt/leshto-batt -name '__pycache__' -delete

rm $HOME/.local/opt/leshto-batt/ref.txt
rm $HOME/.local/opt/leshto-batt/plugin/battlimiter-iface.sh
rm -r $HOME/.local/opt/leshto-batt/service

echo "Para instalar o plugin de conservação de bateria do Ideapad, é necessário se autenticar como super usuário."
sudo sh -c "cp src/plugin/battlimiter-iface.sh /usr/local/bin/ && chmod +x /usr/local/bin/battlimiter-iface.sh"
if [[ $? -ne 0 ]]; then
    echo "Erro ao instalar o plugin: nenhum suporte à conservação de bateria do Ideapad até instalar"
    echo "Instale o plugin manualmente com o comando:"
    echo "  $ sudo sh -c \"cp src/plugin/battlimiter-iface.sh /usr/local/bin/ && chmod +x /usr/local/bin/battlimiter-iface.sh\""
else
  echo "Deseja instalar o serviço de conservação de bateria do Leshto Batt para limitar o carregamento para 70-80%? [sim/não]"
  read response
  if [ "$response" = "sim" ]; then
    sudo sh -c "cp src/service/battlimiter-service.sh /usr/local/bin/ && chmod +x /usr/local/bin/battlimiter-service.sh"
    sudo cp src/service/battlimiter.service /etc/systemd/system/
    echo "Ativar serviço"
    sudo systemctl enable battlimiter.service
    sudo systemctl start battlimiter.service
  fi
fi

echo "OK"
echo "por tales"
