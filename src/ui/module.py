#   Leshto Batt module.py - Classe base para os módulos gráficos do software.
#   Copyright (C) 2021-2024 Cledson Ferreira <cledsonitgames@gmail.com>

#    This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

from os.path import join
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

# Leia /design/modules/IDEIA.txt

class Module():
    """Classe genérica de módulos"""
    def __init__(self, path='', name='module', ext='.ui'):
        self.builder = Gtk.Builder()
        fpath = join(path, name+ext)
        self.builder.add_from_file(fpath)
        self.widget = self.builder.get_object(name)
        self.screen = Gdk.Display.get_default_screen(Gdk.Display.get_default())
        # o nome do arquivo sem a extensão deve ser o mesmo
        # ... do objeto principal
    
    def run(self):
        """Executa as threads do módulo"""
        pass
    
    def stop(self):
        """Para as threads do módulo"""
        pass
