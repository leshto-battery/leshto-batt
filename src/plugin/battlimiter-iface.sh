#!/bin/bash

#   Leshto Batt battlimiter-iface.sh - Plugin BatteryChargeLimiter do Leshto Batt para o Ideapad.
#   Copyright (C) 2024 Cledson Ferreira <cledsonitgames@gmail.com>

#    This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# The script allows managing Lenovo Ideapad's firmware on the battery
# and shifts between the usual charging thresholds from 95/100% to 55/60%.
# In some cases, not charging the battery to 100% constantly may improve
# the overall lifespan of the battery.
# This setting is suggested for a system that is always plugged into
# the AC adapter.
#
# Authors:    Cledson Ferreira <cledsonitgames@gmail.com> and Lenovsky <lenovsky@pm.me>
# Source:     https://gitlab.com/leshto-battery/leshto-batt
# Version:    0.2.0

readonly VERSION=0.2.0
readonly BATTERY_PROTECTION='/sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/conservation_mode'
readonly BATTERY_PROTECTION_ON=1
readonly BATTERY_PROTECTION_OFF=0

usage() {
    # Prints general usage of the script.

    cat <<EOF
usage: ${0##*/} [operation]
operations:
  -h, --help        Show this message.
  -v, --version     Show script version.
  -s, --status      Show battery protection status.
  -e, --enable      Enable battery protection (charge level 55-60%).
  -d, --disable     Disable battery protection (charge level 100%).
EOF
}

die() {
    # Raises an error message and exits.
    # param: $1: $error_message: error message

    local exit_code=-1
    local error_message="$1"

    echo "${0##*/}: ${error_message}" 1>&2

    exit "${exit_code}"
}

root_check() {
    # Checks if the script is called as root,
    # if not raises an error message and dies.

    [[ "$EUID" -ne 0 ]] &&
        die 'You cannot perform this operation unless you are root.'
}

battery_protection_show() {
    # Shows battery protection status based on $BATTERY_PROTECTION value.

    local status="$(cat "${BATTERY_PROTECTION}")"

    if [[ "${status}" -eq "${BATTERY_PROTECTION_ON}" ]]; then
        # echo "Battery protection: ENABLED"
        exit 1
    elif [[ "${status}" -eq "${BATTERY_PROTECTION_OFF}" ]]; then
        # echo "Battery protection: DISABLED"
        exit 0
    else
        # echo "Battery protection: UNKNOWN"
        exit -1
    fi
}

battery_protection_store() {
    # Stores value in $BATTERY_PROTECTION
    # param: $1: $store: values for $BATTERY_PROTECTION, where:
    #   * 1 -> enabled
    #	* 0 -> disabled

    local store="$1"

    if [[ "${store}" -eq "${BATTERY_PROTECTION_ON}" ]]; then
        echo 'Battery protection enabled (charge level 55-60%)'
        echo "${store}" >"${BATTERY_PROTECTION}"
    elif [[ "${store}" -eq "${BATTERY_PROTECTION_OFF}" ]]; then
        echo 'Battery protection disabled (charge level 100%)'
        echo "${store}" >"${BATTERY_PROTECTION}"
    else
        die "Invalid value encountered in 'battery_protection_store()'"
    fi
}

main() {
    # Parses command-line arguments in order to perform the stuff.
    # param: $1: $key: key to the stuff

    local key="$1"

    case "${key}" in
    -h | --help)
        usage
        ;;
    -v | --version)
        echo "${0##*/} v${VERSION}"
        ;;
    -s | --status)
        battery_protection_show
        ;;
    -e | --enable)
        root_check
        battery_protection_store "${BATTERY_PROTECTION_ON}"
        ;;
    -d | --disable)
        root_check
        battery_protection_store "${BATTERY_PROTECTION_OFF}"
        ;;
    "")
        die "No operation specified. See '${0##*/} --help'."
        ;;
    *)
        die "Invaild operation '${key}'. See '${0##*/} --help'."
        ;;
    esac
}

main "$@"
exit 0
